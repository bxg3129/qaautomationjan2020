package com.cerotid.junitdemo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculatorTest {
	//Member variable
	Calculator calc;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		//Instantiation ==> Object creation
		calc = new Calculator();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddBothPositives() {
		assertEquals(15, calc.add(5, 10));
	}
	
	@Test
	public void testAddBothNegatives() {
		
		assertEquals(-6, calc.add(-5,-1));
	}
	
	@Test
	public void testSubtract() {
		assertEquals(8, calc.subtract(10, 2));
		assertEquals(-2, calc.subtract(0, 2));
		assertEquals(0, calc.subtract(0, 0));
		assertEquals(-12, calc.subtract(-10, 2));
		assertEquals(-8, calc.subtract(-10, -2));
	}

}
