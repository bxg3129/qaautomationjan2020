package com.qaautomation.general;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JunitAnnotationsTest {
	@AfterClass
	public static void runAfterClass() {
		System.out.println("Runnign after class");
	}
	
	@BeforeClass
	public static void methodRunBeforeClass() {
		System.out.println("Printing from Before class method");
	}
	
	@Before
	public void setUp() {
		System.out.println("This is from setup");
	}
	
	@Test
	public void someTest() {
		System.out.println("I am from Test method");
	}
	
	@Test
	public void someMoreTest() {
		System.out.println("I am from Some More Test method");
	}
	
	@After
	public void runAfterTest() {
		System.out.println("Running after tests");
	}
	
	
}
