package com.cerotid.junitdemo;

public class Calculator {
	
	//modifier ==> public, private, protected, default
	public int add(int i, int j){
		return i + j;
	}
	
	public int subtract(int a, int b) {
		return a - b;
	}
	
	//Inner class
	//TODO class to go over inner classes
	private class SomeClass {
		
	}
}
