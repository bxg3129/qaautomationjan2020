package com.cerotid.celena;

public class LoopAssignment {

	public static void main(String[] args) {
		printLeftAlignedStars();

		printSumFromOneToHundred();
		
		printCenterAlignedStars();
	}


	private static void printCenterAlignedStars() {
		// TODO Ruza: Need to put Center aligned stars logic and print them
		
	}


	private static void printLeftAlignedStars() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println();// new line
		}
	}

	private static void printSumFromOneToHundred() {
		int sum = 0;
		for (int i = 1; i <= 100; i++) {
			sum = i + sum;
		}
		System.out.println("Total sum:" + sum);
	}
}