package com.cerotid.qaautomation;

import com.cerotid.qaautomation.model.Vehicle;

public class ArrayConcept {

	public static void main(String[] args) {
		showMeHowArrayWorks();
		showMeHowMultiDimensionalArrayWorks();
		showMeHowArrayWorksWithMyObjects();
	}

	private static void showMeHowArrayWorksWithMyObjects() {
		// TODO Auto-generated method stub
		/*
		 * Creating Object of type Vehicle
		 * Also called "instantiation" or Instantiate an Object for a class
		 * 
		 */
		Vehicle vehicle1 = new Vehicle();
		vehicle1.setMake("Honda");
		vehicle1.setModel("Civic");
		vehicle1.setVin("1232hsdfhsadf234345");
		vehicle1.setYear("2019");
		vehicle1.setColor("Red");
		
		Vehicle vehicle2 = new Vehicle();
		vehicle2.setMake("Toyota");
		vehicle2.setModel("Highlander");
		vehicle2.setVin("2345jsdfbawrfeswa2354");
		vehicle2.setYear("2020");
		vehicle2.setColor("Black");
		
		Vehicle vehicle3 = new Vehicle();
		vehicle3.setColor("Red");
		
		Vehicle vehicle4 = new Vehicle();
		vehicle4.setColor("Orange");
		
		Vehicle vehicle5 = new Vehicle();
		vehicle5.setColor("Red");
		
		//datatype [] variableName = new datatype [size of Collection]
		Vehicle [] vehicleArray = new Vehicle [5];
		vehicleArray[0] = vehicle1;
		vehicleArray[1] = vehicle2;
		vehicleArray[2] = vehicle3;
		vehicleArray[3] = vehicle5;
		vehicleArray[4] = vehicle4;
		
		paintAllVehiclesColorOrange(vehicleArray);
		
		System.out.println("Yay! I got my cars back! Lets see their colors now!");
		
		//Advance for loop
		for(Vehicle vehicle: vehicleArray) {
			System.out.println(vehicle);
		}
	}

	private static void paintAllVehiclesColorOrange(Vehicle[] vehArr) {
		for(int i =0; i < vehArr.length; i++) {
			if(!("Orange").equals(vehArr[i].getColor())) {
				vehArr[i].setColor("Orange");
			}
		}
	}

	private static void showMeHowMultiDimensionalArrayWorks() {
		String [][] stateMapArray = new String[5][2];
		
		stateMapArray[0][0] = "TX";
		stateMapArray[0][1] = "Texas";
		
		stateMapArray[1][0] = "CA";
		stateMapArray[1][1] = "California";
		
		stateMapArray[2][0] = "CO";
		stateMapArray[2][1] = "Colorado";
		
		stateMapArray[3][0] = "OH";
		stateMapArray[3][1] = "Ohio";
		
		stateMapArray[4][0] = "WA";
		stateMapArray[4][1] = "Washington";
		
		System.out.println("State Map Array Length: " + stateMapArray.length);
		
		System.out.println("StateMap Rows Number: " + stateMapArray[0].length);
		int rows = stateMapArray[0].length;
		
		for (int i = 0; i< stateMapArray.length; i++) {
			for(int j = 0; j < rows; j ++ ) {
				System.out.println(stateMapArray[i][j]);
			}
		}
	}

	private static void showMeHowArrayWorks() {
		// Array - to store collection of elements
		int[] intArray = { 1, 3, 4, 5, 8, 9 };

		System.out.println("Size of intArray is: " + intArray.length);
		System.out.println("Element in second postion in intArray is: " + intArray[1]);

		System.out.println("\nLets traverse intArray from the beginning");
		// Lets iterate over all the elements in intArray
		for (int i = 0; i < intArray.length; i++) {
			System.out.print(intArray[i]);
		}
		
		System.out.println("\nLets traverse intArray from opposite direction");

		// Lets iterate over all the elements in intArray in reverse Order
		for (int i = intArray.length - 1; i >= 0; i--) {
			System.out.print(intArray[i]);
		}
		
		manipulateCollections(intArray);
		
		System.out.println("Element in second postion in intArray is: " + intArray[1]);

	}

	private static void manipulateCollections(int[] array) {
		System.out.println("\nLets see what size of Array I received." );
		System.out.println("I receive array of size  " + array.length);
		
		System.out.println("Before change; Element in second postion in array  is: " + array[1]);
		
		array[1] = 100;
		
		array[0] = 0;
	}

}
