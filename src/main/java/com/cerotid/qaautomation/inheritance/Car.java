package com.cerotid.qaautomation.inheritance;

public abstract class Car {
	//variables--> member variables
	//instance variables
	//fields
	protected Tire tire;
	protected Engine engine;
	protected int doors;
	protected CarFeatures carfeatures;
	
	//default constructor
	public Car() {
		
	}

	//overloaded constructor
	//parameterized constructor
	public Car(Tire tire, Engine engine, int doors) {
		super();
		this.tire = tire;
		this.engine = engine;
		this.doors = doors;
		
		this.carfeatures = new CarFeatures();
	}
	
	//overloaded constructor
		//parameterized constructor
		public Car(Tire tire, Engine engine, int doors, CarFeatures carFeatures) {
			super();
			this.tire = tire;
			this.engine = engine;
			this.doors = doors;
			this.carfeatures = carFeatures;
		}
	
	public abstract double getMaxSpeed();
	
	public boolean hasAndroidAuto() {
		return true;
	}
	
	public boolean hasCarPlay() {
		return true;
	}
	
	
	public Tire getTire() {
		return tire;
	}

	public void setTire(Tire tire) {
		this.tire = tire;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public int getDoors() {
		return doors;
	}

	public void setDoors(int doors) {
		this.doors = doors;
	}
	
	public CarFeatures getCarfeatures() {
		return carfeatures;
	}

	public void setCarfeatures(CarFeatures carfeatures) {
		this.carfeatures = carfeatures;
	}

	@Override
	public String toString() {
		return "Car [tire=" + tire + ", engine=" + engine + ", doors=" + doors + "]";
	}
	
	
}
