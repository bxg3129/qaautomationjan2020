package com.cerotid.qaautomation.inheritance;

public class CarFeatures {
	private boolean hasMoonRoof;
	private boolean heatedCarSeat;
	private boolean hasBackCamera;
	private boolean breakSensors;
	private boolean hasTechnologyPackage;
	
	
	public CarFeatures() {
		super();
		// TODO Auto-generated constructor stub
		this.hasBackCamera = true;
	}
	
	public CarFeatures(boolean hasMoonRoof, boolean heatedCarSeat, boolean hasBackCamera, boolean breakSensors,
			boolean hasTechnologyPackage) {
		super();
		this.hasMoonRoof = hasMoonRoof;
		this.heatedCarSeat = heatedCarSeat;
		this.hasBackCamera = hasBackCamera;
		this.breakSensors = breakSensors;
		this.hasTechnologyPackage = hasTechnologyPackage;
	}
	
	public boolean isHasMoonRoof() {
		return hasMoonRoof;
	}
	public void setHasMoonRoof(boolean hasMoonRoof) {
		this.hasMoonRoof = hasMoonRoof;
	}
	public boolean isHeatedCarSeat() {
		return heatedCarSeat;
	}
	public void setHeatedCarSeat(boolean heatedCarSeat) {
		this.heatedCarSeat = heatedCarSeat;
	}
	public boolean isHasBackCamera() {
		return hasBackCamera;
	}
	public void setHasBackCamera(boolean hasBackCamera) {
		this.hasBackCamera = hasBackCamera;
	}
	public boolean isBreakSensors() {
		return breakSensors;
	}
	public void setBreakSensors(boolean breakSensors) {
		this.breakSensors = breakSensors;
	}

	public boolean isHasTechnologyPackage() {
		return hasTechnologyPackage;
	}

	public void setHasTechnologyPackage(boolean hasTechnologyPackage) {
		this.hasTechnologyPackage = hasTechnologyPackage;
	}
	
	


}
