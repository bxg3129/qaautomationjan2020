package com.cerotid.qaautomation;

public class PassByValue {
	public static void main(String[] args) {
		String str = "Sam";
		
		modifyString(str);
		
		System.out.println("Checking if its reference or pass by value: " +str);
	}

	private static void modifyString(String str) {
		System.out.println("Right before modification: " + str);
		str = "David";
		
		System.out.println("After reassignment: " + str);
	}
}
