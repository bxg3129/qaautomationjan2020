package com.cerotid.qaautomation;

public class LoopConcept {
	static int count;

	public static void main(String[] args) {
		showWhileLoopConcept();

		printTotalLoopRunCount();

		showForLoopConcept();

		printTotalLoopRunCount();
		
		showDoWhileConcept();
		
		printTotalLoopRunCount();
		
		showMeHowNestedLoopWorks();
		
		showMeHowThreeNestedLoopWorks();
	}

	private static void showMeHowThreeNestedLoopWorks() {
		System.out.println("We are crazy- Looking at 3 nested loop");
		for(int i = 10; i >= 1; i--) {
			for(int j = 10; j >= 1; j--) {
				for(int k = 10; k >= 1; k--) {
					System.out.println(i + "X" + j +"X"+ k + "="+ (i*j*k));
				}
			}
		}
		
	}

	private static void showMeHowNestedLoopWorks() {
		for(int i = 1; i <= 10; i++) {
			System.out.println("Multiplication Table of: " + i);
			for(int j = 1; j <= 10; j++) {
				System.out.println(i + "X" + j + "="+ (i*j));
			}
		}
		
	}

	private static void printTotalLoopRunCount() {
		int loopCount = showMeHowManyTimesAllTheLoopsRan();
		System.out.println("Times loop behaviors ran: " + loopCount);

	}

	static int showMeHowManyTimesAllTheLoopsRan() {
		return count;
	}

	static void showWhileLoopConcept() {
		/*
		 * Syntax: while (boolean condition) { loop statements.. }
		 */
		int index = 1;

		while (index < 6) {
			System.out.println("My Name is Bishal");
			index = index + 1; // index++
		}
		count++; // count = count + 1;
	}

	static void showForLoopConcept() {
		/*
		 * for(initialization condition; testing condition; increment/decrement) { loop
		 * statement(s)... }
		 */

		for (int index = 1; index < 6; index++) {
			System.out.println("My name is Ruza");
		}

		count++;
	}

	static void showDoWhileConcept() {
		/*
		 * do { statement(s).. } while (boolean condition);
		 */
		int index = 1;
		do {
			System.out.println("My name is Srijana");
			index++;
		} while (index < 6);
		
		count++;
	}
}
