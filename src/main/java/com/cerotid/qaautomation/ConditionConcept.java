package com.cerotid.qaautomation;

public class ConditionConcept {

	public static void main(String[] args) {
		showMeHowIfConditionWorks();
		
		showMeHowIfElseConditionWorks();
		
		showMeHowIfElseIfConditionWorks();
		
		showMeHowSwitchCaseConditionWorks();
	}

	private static void showMeHowSwitchCaseConditionWorks() {
		// TODO Auto-generated method stub
		//RED, GREEN,BLUE
		System.out.println("Printing Primary Color of user's choice");
		String key ="T";
		
		switch (key) {
		case "R":
			System.out.println("You chose RED");
			break;
		case "G":
			System.out.println("You chose GREEN");
			break;	
		case "B":
			System.out.println("You chose BLUE");
			break;
		default:
			System.out.println("There is no such primary Color");
			break;
		}
		
	}

	private static void showMeHowIfElseIfConditionWorks() {
		/*
		 * if(boolean condition)
		 * {
		 *    statements
		 * }
		 * else if (boolean condition)
		 * {
		 * 
		 * }
		 * ..
		 * ..
		 * [optional] else{
		 * 
		 * }
		 * 
		 */
		
		int age = 19;
		System.out.println("Looking at Voting, DriverLicence & Alcohol purchase rights for a person of age: " + age);
		
		if(age >= 21) {
			System.out.println("You can drive, vote and purchase Alcohol");
		}
		else if (age < 21 && age >=18 ) {
			System.out.println("You can drive and Vote!");
		}
		else if (age < 18 && age >=16) {
			System.out.println("You can Drive!");
		}
		else {
			System.out.println("Have parental consent!");
		}
			
	
		
	}

	private static void showMeHowIfElseConditionWorks() {
		/*
		 * if(boolean condition)
		 * {
		 * 	   statement(s)
		 * }
		 * else {
		 *     statements (s)
		 * 
		 * }
		 */
		
		int passingScore = 60;
		
		int yourScore = 59;
		
		System.out.println("Your score is:"+ yourScore);
		
		if(yourScore >= passingScore)
			System.out.println("Congratulations! You passed!");
		else
			System.out.println("Sorry! You failed this time.");
				
		
		
		
	}

	private static void showMeHowIfConditionWorks() {
		/*
		 * Syntax:
		 * if(boolean condition){
		 * 			statement(s)
		 * }
		 */
		int age = 19;
		System.out.println("A person age " + age + " walks into a Store!");
		
		if(age >=21) {
			System.out.println("Lottery & Alcohol sold!");
			
			return;
		}
		
		if(age >= 18) {
			System.out.println("Lottery sold!");
			
			return;
		}
		
		System.out.println("The person walked out with nothing!");
	
	}

}
