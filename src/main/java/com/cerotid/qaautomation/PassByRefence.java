package com.cerotid.qaautomation;

public class PassByRefence {
	public static void main(String[] args) {
		String [] strArray = {"Sam", "David", "Erol"};
		
		System.out.println("Element in first position :" + strArray[0]);
		
		modifyArray(strArray);
		
		System.out.println("Element in first position :" + strArray[0]);
	}

	private static void modifyArray(String[] stringArray) {
		stringArray[0] = "Celena";
		
		
	}
	
}
