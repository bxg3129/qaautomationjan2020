package com.cerotid.qaautomation;

public class StringConcept {
	public static void main(String[] args) {
		String firstName = "Bishal";
		String lastName = "Ghimire";
		
		//contatenation
		String fullName = firstName + " " + lastName;
		
		System.out.println("Full Name is "+ fullName);
		
		System.out.println("First Name in upper case: " + firstName.toUpperCase());
		System.out.println("Substring feature of String: " + firstName.substring(0, 4));
		System.out.println(firstName.concat(" ").concat(lastName).concat(" is awesome!"));
		System.out.println("Length of my first Name is: " + firstName.length());
	
	}
}
