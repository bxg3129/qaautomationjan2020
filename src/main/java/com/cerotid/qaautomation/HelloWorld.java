package com.cerotid.qaautomation;

public class HelloWorld{
	static int i;
	static char c;
	static short s;
	static boolean b;
	static double d;
	static float f;
	static long l;
	static String str;
	static Integer intgr;
	
	//method
	public static void main(String[] args) {
		//printing default vaules
		System.out.println(i);
		System.out.println(c);
		System.out.println(s);
		System.out.println(b);
		System.out.println(d);
		System.out.println(f);
		System.out.println(l);
		System.out.println(str);
		System.out.println(intgr);
		
		String myName = "Bishal";
		
		System.out.println("My name is " + myName);
		
		int sum = 5 + i;
		
		System.out.println("Sum result is: " + sum);


	}

}
